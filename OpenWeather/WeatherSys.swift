//
//  WeatherSys.swift
//  OpenWeather
//
//  Created by Rémi Hillairet on 8/13/16.
//  Copyright © 2016 Remi Hillairet. All rights reserved.
//

import ObjectMapper

class WeatherSys: Mappable, CustomDebugStringConvertible {
    var country: String?
    var sunrise: Int?
    var sunset: Int?
    
    var debugDescription: String {
        return "\n\tcountry: \(country)\n\tsunrise: \(sunrise)\n\tsunset:\(sunset)"
    }

    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        country <- map["country"]
        sunrise <- map["sunrise"]
        sunset <- map["sunset"]
    }
}
