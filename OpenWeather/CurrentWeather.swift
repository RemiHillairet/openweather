//
//  CurrentWeather.swift
//  OpenWeather
//
//  Created by Rémi Hillairet on 8/13/16.
//  Copyright © 2016 Remi Hillairet. All rights reserved.
//

import ObjectMapper

class CurrentWeather: Mappable, CustomDebugStringConvertible {
    var weather: [Weather]?
    var main: WeatherMainData?
    var wind: WeatherWind?
    var clouds: Int?
    var rain: Int?
    var snow: Int?
    var dt: Int?
    var sys: WeatherSys?
    var id: Int?
    var name: String?
    
    var debugDescription: String {
        return "id: \(id)\nName: \(name)\nWeather:\(weather)\nMain:\(main)\nWind:\(wind)\nClouds: \(clouds)\nRain: \(rain)\nSnow: \(snow)\nDt: \(dt)\nSys:\(sys)"
    }
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        weather <- map["weather"]
        main <- map["main"]
        wind <- map["wind"]
        clouds <- map["clouds"]["all"]
        rain <- map["rain"]["3h"]
        snow <- map["snow"]["3h"]
        dt <- map["dt"]
        sys <- map["sys"]
        id <- map["id"]
        name <- map["name"]
    }
}
