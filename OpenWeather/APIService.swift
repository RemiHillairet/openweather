//
//  APIService.swift
//  OpenWeather
//
//  Created by Rémi Hillairet on 8/13/16.
//  Copyright © 2016 Remi Hillairet. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import AlamofireImage

class APIService {
    
    class func getCurrentWeather(cityName name: String, completion: (success: Bool, currentWeather: CurrentWeather?) -> Void) {
        let parameters = ["q": name,
                          "APPID": kApiKey]
        Alamofire.request(.GET, kGetCurrentWeather, parameters: parameters)
            .responseObject { (response: Response<CurrentWeather, NSError>) in
                let currentWeather = response.result.value
                if currentWeather != nil && currentWeather!.id != nil {
                    completion(success: true, currentWeather: currentWeather)
                } else {
                    completion(success: false, currentWeather: nil)
                }
        }
    }
    
    class func getCurrentWeather(cityId id: Int, completion: (success: Bool, currentWeather: CurrentWeather?) -> Void) {
        let parameters = ["id": "\(id)",
                          "APPID": kApiKey]
        Alamofire.request(.GET, kGetCurrentWeather, parameters: parameters)
            .responseObject { (response: Response<CurrentWeather, NSError>) in
                let currentWeather = response.result.value
                if currentWeather != nil {
                    completion(success: true, currentWeather: currentWeather)
                } else {
                    completion(success: false, currentWeather: nil)
                }
            }
    }
    
    class func getCurrentWeather(lat lat: Double, lon: Double, completion: (success: Bool, currentWeather: CurrentWeather?) -> Void) {
        let parameters = ["lat": "\(lat)",
                          "lon": "\(lon)",
                          "APPID": kApiKey]
        Alamofire.request(.GET, kGetCurrentWeather, parameters: parameters)
            .responseObject { (response: Response<CurrentWeather, NSError>) in
                let currentWeather = response.result.value
                if currentWeather != nil {
                    completion(success: true, currentWeather: currentWeather)
                } else {
                    completion(success: false, currentWeather: nil)
                }
        }
    }
    
    class func searchCityPhoto(name: String, completion: (success: Bool, cityPhoto: UIImage?) -> Void) {
        let parameters = ["format": "json",
                          "api_key": kFlickrApiKey,
                          "sort": "relevance",
                          "content_type": "1",
                          "per_page": "1",
                          "nojsoncallback": "?",
                          "text": name]
        Alamofire.request(.GET, kFlickrPhotosSearch, parameters: parameters)
            .responseArray(keyPath: "photos.photo" ){ (response: Response<[FlickrPhoto], NSError>) in
                if let photos = response.result.value {
                    if photos.count >= 1 {
                        let photo = photos[0]
                        guard let farmId = photo.farm, let serverId = photo.server, let id = photo.id, let secret = photo.secret else {
                            completion(success: false, cityPhoto: nil)
                            return
                        }
                        let photoURL = "https://farm\(farmId).staticflickr.com/\(serverId)/\(id)_\(secret)_b.jpg"
                        Alamofire.request(.GET, photoURL)
                            .responseImage(completionHandler: { (response: Response<UIImage, NSError>) in
                                if let image = response.result.value {
                                    completion(success: true, cityPhoto: image)
                                }
                            })
                    }
                } else {
                    completion(success: false, cityPhoto: nil)
                }
            }
    }
}
