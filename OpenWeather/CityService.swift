//
//  CityService.swift
//  OpenWeather
//
//  Created by Rémi Hillairet on 8/14/16.
//  Copyright © 2016 Remi Hillairet. All rights reserved.
//

import UIKit
import SwiftyJSON

class CityService {
    
    static let sharedInstance = CityService()
    
    private var cityList: [City] = []
    
    private func getCitiesFromJSON() {
        self.cityList.removeAll()
        guard let path = NSBundle.mainBundle().pathForResource("city.list", ofType: "json") else {
            return
        }
        print(path)
        do {
            let jsonData = try NSData(contentsOfFile: path, options: .DataReadingMappedAlways)
            let json = JSON(data: jsonData)
            for i in 0..<(json.count) {
                let id: Int = json[i]["_id"].intValue
                let name: String = json[i]["name"].stringValue
                //let country: String = json[i]["country"].stringValue
                let newCity = City(id: id, name: name)
                self.cityList.append(newCity)
            }
        } catch {
            print("Reading JSON file failed!")
            return
        }
    }
    
    class func searchCity(name: String) -> City? {
        return CityService.sharedInstance.searchCity(name)
    }
    
    private func searchCity(name: String) -> City? {
        if cityList.isEmpty {
            self.getCitiesFromJSON()
        }
        print(self.cityList)
        return nil
    }
}
