//
//  LocationService.swift
//  OpenWeather
//
//  Created by Rémi Hillairet on 8/14/16.
//  Copyright © 2016 Remi Hillairet. All rights reserved.
//

import UIKit
import CoreLocation

class LocationService: NSObject, CLLocationManagerDelegate {
    
    static let sharedInstance = LocationService()
    
    let locationManager = CLLocationManager()
    var lastLocation: CLLocation?
    
    override init() {
        super.init()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if (status == .AuthorizedWhenInUse) || (status == .AuthorizedAlways) {
            self.locationManager.startUpdatingLocation()
        } else if (status == .Restricted) || (status == .Denied) {
            NSNotificationCenter.defaultCenter().postNotificationName("LocationNotAuthorized", object: nil)
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
        if newLocation.verticalAccuracy <= 100.00 && newLocation.horizontalAccuracy <= 100.00 {
            self.locationManager.stopUpdatingLocation()
            if self.lastLocation == nil {
                self.lastLocation = newLocation
                NSNotificationCenter.defaultCenter().postNotificationName("LocationDidUpdate", object: nil)
            }
        }
    }
    
    class func getCurrentLocation() {
        return LocationService.sharedInstance.getCurrentLocation()
    }
    
    private func getCurrentLocation() {
        switch CLLocationManager.authorizationStatus() {
        case .AuthorizedWhenInUse, .AuthorizedAlways:
            if self.lastLocation == nil || (self.lastLocation?.timestamp.timeIntervalSinceNow > 300.00) {
                self.lastLocation = nil
                self.locationManager.startUpdatingLocation()
            } else {
                NSNotificationCenter.defaultCenter().postNotificationName("LocationDidUpdate", object: nil)
            }
        case .NotDetermined:
            self.locationManager.requestWhenInUseAuthorization()
        case .Restricted, .Denied:
            NSNotificationCenter.defaultCenter().postNotificationName("LocationNotAuthorized", object: nil)
        }
    }
}
