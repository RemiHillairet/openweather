//
//  HomeViewController.swift
//  OpenWeather
//
//  Created by Rémi Hillairet on 8/13/16.
//  Copyright © 2016 Remi Hillairet. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData

class HomeViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    var cityList: [City] = []
    var cityViewControllers: [UIViewController] = []
    var currentViewController: UIViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Navigation Bar with translucent black
        let color = UIColor(white: 0.0, alpha: 0.40)
        let navbarBackground44 = Helper.navbarBackgroundImage(color)
        let navbarBackground32 = Helper.navbarBackgroundImage(color)
        self.navigationController?.navigationBar.setBackgroundImage(navbarBackground44, forBarMetrics: .Default)
        self.navigationController?.navigationBar.setBackgroundImage(navbarBackground32, forBarMetrics: .Compact)
        self.navigationController?.navigationBar.barStyle = .Default
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        
        self.dataSource = self
        self.delegate = self
        self.automaticallyAdjustsScrollViewInsets = false
        self.view.backgroundColor = UIColor.blackColor()
        self.initViewControllers()
    }
    
    func initViewControllers() {
        // Add user location weather viewController
        if let userLocationVC = self.newWeatherViewController() as? WeatherViewController {
            userLocationVC.isUserLocation = true
            self.cityViewControllers.append(userLocationVC)
        }
        self.fetchCities()
        for city in self.cityList {
            if let newVC = self.newWeatherViewController() as? WeatherViewController {
                newVC.city = city
                self.cityViewControllers.append(newVC)
            }
        }
        if let firstVC = self.cityViewControllers.first {
            self.setViewControllers([firstVC], direction: .Forward, animated: true, completion: nil)
        }
    }
    
    func newWeatherViewController() -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("WeatherViewController")
    }
    
    override func viewWillAppear(animated: Bool) {
    }
    
    override func viewDidAppear(animated: Bool) {
    }
    
    // MARK: - UIPageViewControllerDataSource
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        guard let pageIndex = self.cityViewControllers.indexOf(viewController) else {
            return nil
        }
        return pageIndex > 0 ? self.cityViewControllers[pageIndex - 1] : nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        guard let pageIndex = self.cityViewControllers.indexOf(viewController) else {
            return nil
        }
        return pageIndex < (self.cityViewControllers.count - 1) ? self.cityViewControllers[pageIndex + 1] : nil
    }

    // MARK: - IBAction
    
    @IBAction func addCityAction(sender: AnyObject) {
        let alertController = UIAlertController(title: "Add a city", message: "Enter the city's name", preferredStyle: .Alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alertController.addAction(cancelAction)
        let addCityAction = UIAlertAction(title: "Add", style: .Default) { (alertAction: UIAlertAction) in
            let cityNameTextField = alertController.textFields![0] as UITextField
            self.addCity(cityNameTextField.text!)
        }
        alertController.addAction(addCityAction)
        alertController.addTextFieldWithConfigurationHandler { (textfield: UITextField) in
            textfield.autocapitalizationType = .Words
        }
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func addCity(name: String) {
        APIService.getCurrentWeather(cityName: name) { (success, currentWeather) in
            if success, let currentWeather = currentWeather {
                guard let id = currentWeather.id, let name = currentWeather.name else {
                    return
                }
                if self.cityList.indexOf({$0.id == id}) != nil {
                    let alertController = UIAlertController(title: "Already added", message: "This city has already been added", preferredStyle: .Alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                    alertController.addAction(defaultAction)
                    self.presentViewController(alertController, animated: true, completion: nil)
                    return
                }
                let newCity = City(id: id, name: name)
                self.cityList.append(newCity)
                self.saveCity(newCity)
                if let newVC = self.newWeatherViewController() as? WeatherViewController {
                    newVC.city = newCity
                    self.cityViewControllers.append(newVC)
                    self.setViewControllers([newVC], direction: .Forward, animated: true, completion: nil)
                }
            } else {
                let alertController = UIAlertController(title: "No city found", message: "Sorry no city found with this name", preferredStyle: .Alert)
                let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alertController.addAction(defaultAction)
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func removeCityAction(sender: AnyObject) {
        if let selectedIndex = self.cityViewControllers.indexOf(self.viewControllers![0]) {
            if selectedIndex > 0 {
                self.cityViewControllers.removeAtIndex(selectedIndex)
                let city = self.cityList[selectedIndex - 1]
                self.removeCity(city)
                self.cityList.removeAtIndex(selectedIndex - 1)
                self.setViewControllers([self.cityViewControllers[selectedIndex - 1]], direction: .Reverse, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - CoreData Management
    
    func saveCity(city: City) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let entity =  NSEntityDescription.entityForName("Cities", inManagedObjectContext:managedContext)
        let cityEntity = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
        cityEntity.setValue(city.id, forKey: "id")
        cityEntity.setValue(city.name, forKey: "name")
        do {
            try managedContext.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    func removeCity(city: City) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "Cities")
        fetchRequest.predicate = NSPredicate(format: "id == %d", city.id)
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            let cities = results as! [NSManagedObject]
            if cities.count >= 1 {
                managedContext.deleteObject(cities[0])
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func fetchCities() {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "Cities")
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            let cities = results as! [NSManagedObject]
            for cityEntity in cities {
                guard let id = cityEntity.valueForKey("id") as? Int else {
                    continue
                }
                guard let name = cityEntity.valueForKey("name") as? String else {
                    continue
                }
                let city = City(id: id, name: name)
                self.cityList.append(city)
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    // MARK: - Memory
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
