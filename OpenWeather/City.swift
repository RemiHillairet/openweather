//
//  City.swift
//  OpenWeather
//
//  Created by Rémi Hillairet on 8/14/16.
//  Copyright © 2016 Remi Hillairet. All rights reserved.
//

struct City {
    var id: Int
    var name: String
}
