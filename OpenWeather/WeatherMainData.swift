//
//  WeatherMainData.swift
//  OpenWeather
//
//  Created by Rémi Hillairet on 8/13/16.
//  Copyright © 2016 Remi Hillairet. All rights reserved.
//

import ObjectMapper

class WeatherMainData: Mappable, CustomDebugStringConvertible {
    var temp: Double?
    var pressure: Int?
    var humidity: Int?
    var tempMin: Double?
    var tempMax: Double?
    var seaLevel: Int?
    var grndLevel: Int?
    
    var debugDescription: String {
        return "\n\ttemp: \(temp)\n\tpressure: \(pressure)\n\thumidity: \(humidity)\n\ttemp_min: \(tempMin)\n\ttemp_max: \(tempMax)\n\tsea_level: \(seaLevel)\n\tgrnd_level: \(grndLevel)"
    }

    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        temp <- map["temp"]
        pressure <- map["pressure"]
        humidity <- map["humidity"]
        tempMin <- map["temp_min"]
        tempMax <- map["temp_max"]
        seaLevel <- map["sea_level"]
        grndLevel <- map["grnd_level"]
    }
}
