//
//  Helper.swift
//  OpenWeather
//
//  Created by Rémi Hillairet on 8/14/16.
//  Copyright © 2016 Remi Hillairet. All rights reserved.
//

import UIKit
import CoreGraphics

class Helper {
    class func navbarBackgroundImage(color: UIColor) -> UIImage {
        let rect = CGRectMake(0, 0, 1, 1)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
