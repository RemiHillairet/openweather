//
//  WeatherWind.swift
//  OpenWeather
//
//  Created by Rémi Hillairet on 8/13/16.
//  Copyright © 2016 Remi Hillairet. All rights reserved.
//

import ObjectMapper

class WeatherWind: Mappable, CustomDebugStringConvertible {
    var speed: Double?
    var deg: Double?
    
    var debugDescription: String {
        return "\n\tspeed: \(speed)\n\tdeg: \(deg)"
    }

    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        speed <- map["speed"]
        deg <- map["deg"]
    }
}
