//
//  FlickrPhoto.swift
//  OpenWeather
//
//  Created by Rémi Hillairet on 8/15/16.
//  Copyright © 2016 Remi Hillairet. All rights reserved.
//

import ObjectMapper

class FlickrPhoto: Mappable {
    
    var id: String?
    var secret: String?
    var farm: Int?
    var server: String?
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        secret <- map["secret"]
        farm <- map["farm"]
        server <- map["server"]
    }
}
