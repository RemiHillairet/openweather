//
//  WeatherCoordinates.swift
//  OpenWeather
//
//  Created by Rémi Hillairet on 8/13/16.
//  Copyright © 2016 Remi Hillairet. All rights reserved.
//

import ObjectMapper

class WeatherCoordinates: Mappable {
    var lon: Double?
    var lat: Double?

    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        lon <- map["lon"]
        lat <- map["lat"]
    }
}
