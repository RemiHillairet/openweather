//
//  Weather.swift
//  OpenWeather
//
//  Created by Rémi Hillairet on 8/13/16.
//  Copyright © 2016 Remi Hillairet. All rights reserved.
//

import ObjectMapper

class Weather: Mappable, CustomDebugStringConvertible {
    var id: Int?
    var main: String?
    var description: String?
    var icon: String?
    
    var debugDescription: String {
        return "\n\tid: \(id)\n\tmain: \(main)\n\tdescription: \(description)\n\ticon: \(icon)"
    }

    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        main <- map["main"]
        description <- map["description"]
        icon <- map["icon"]
    }
}
