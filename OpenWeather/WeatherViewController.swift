//
//  WeatherViewController.swift
//  OpenWeather
//
//  Created by Rémi Hillairet on 8/14/16.
//  Copyright © 2016 Remi Hillairet. All rights reserved.
//

import UIKit
import CoreLocation

class WeatherViewController: UIViewController {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var cityImageView: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
    @IBOutlet weak var isUserLocationIndicator: UIImageView!
    
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var sunriseLabel: UILabel!
    @IBOutlet weak var sunsetLabel: UILabel!
    
    var city: City?
    var isUserLocation = false
    var currentWeather: CurrentWeather?
    var cityPhoto: UIImage?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.cityImageView.clipsToBounds = true
        self.tempLabel.hidden = true
        self.weatherDescriptionLabel.hidden = true
        if !self.isUserLocation {
            self.isUserLocationIndicator.hidden = true
        }
        self.pressureLabel.text?.removeAll()
        self.humidityLabel.text?.removeAll()
        self.windLabel.text?.removeAll()
        self.sunriseLabel.text?.removeAll()
        self.sunsetLabel.text?.removeAll()
        
        // Add observers for location notifications
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(locationDidUpdate), name: "LocationDidUpdate", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(locationNotAuthorized), name: "LocationNotAuthorized", object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        if self.cityLabel.text == "city_name" {
            self.cityLabel.text = city != nil ? city!.name : "Waiting location..."
        }
        if let city = self.city {
            if cityPhoto == nil {
                self.loadCityPhoto(city.name)
            }
            if self.currentWeather == nil {
                self.getWeatherById(city.id)
            } else {
                self.setWeatherData()
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        if self.isUserLocation && (self.currentWeather == nil) {
            LocationService.getCurrentLocation()
        }
    }
    
    func getWeatherById(id: Int) {
        APIService.getCurrentWeather(cityId: id) { (success: Bool, currentWeather: CurrentWeather?) in
            if success, let currentWeather = currentWeather {
                self.currentWeather = currentWeather
                self.setWeatherData()
            } else {
                print("Failed to load data")
            }
        }
    }
    
    func getWeatherByLocation(lat: Double, lon: Double) {
        APIService.getCurrentWeather(lat: lat, lon: lon, completion: { (success, currentWeather) in
            if success, let currentWeather = currentWeather {
                if currentWeather.id == nil {
                    self.cityLabel.text = "No city found"
                }
                if let cityName = currentWeather.name {
                    self.loadCityPhoto(cityName)
                }
                self.cityLabel.text = currentWeather.name
                self.currentWeather = currentWeather
                self.setWeatherData()
            } else {
                self.cityLabel.text = "No city found"
            }
        })
    }
    
    func setWeatherData() {
        if let temp = self.currentWeather?.main?.temp {
            self.tempLabel.text = String(format: "%dºC", Int(temp - 273.15))
            self.tempLabel.hidden = false
        }
        if let description = self.currentWeather?.weather?[0].description {
            self.weatherDescriptionLabel.text = description
            self.weatherDescriptionLabel.hidden = false
        }
        if let pressure = self.currentWeather?.main?.pressure {
            self.pressureLabel.text = "Pressure: \(pressure) hPa"
        }
        if let humidity = self.currentWeather?.main?.humidity {
            self.humidityLabel.text = "Humidity: \(humidity) %"
        }
        if let wind = self.currentWeather?.wind {
            var windStr = "Wind: "
            if let speed = wind.speed {
                windStr += String(format: "%.2f m/s  ", speed)
            }
            if let degree = wind.deg {
                windStr += String(format: "%.2fº", degree)
            }
            self.windLabel.text = windStr
        }
        let formatter = NSDateFormatter()
        formatter.dateStyle = .NoStyle
        formatter.timeStyle = .ShortStyle
        if let sunrise = self.currentWeather?.sys?.sunrise {
            let date = NSDate(timeIntervalSince1970: Double(sunrise))
            self.sunriseLabel.text = "Sunrise: " + formatter.stringFromDate(date)
        }
        if let sunset = self.currentWeather?.sys?.sunset {
            let date = NSDate(timeIntervalSince1970: Double(sunset))
            self.sunsetLabel.text = "Sunset: " + formatter.stringFromDate(date)
        }
    }
    
    func loadCityPhoto(name: String) {
        APIService.searchCityPhoto(name) { (success, cityPhoto) in
            if success, let cityPhoto = cityPhoto {
                self.cityPhoto = cityPhoto
                self.cityImageView.image = cityPhoto
            }
        }
    }
    
    // MARK - Location Notifications
    
    func locationDidUpdate() {
        if let location = LocationService.sharedInstance.lastLocation {
            self.getWeatherByLocation(location.coordinate.latitude, lon: location.coordinate.longitude)
        }
    }
    
    func locationNotAuthorized() {
        let alertController = UIAlertController(
            title: "Location Access Disabled",
            message: "In order to get the weather from your current location, please open this app's settings and set location access to 'While Using the App'.",
            preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: "Open Settings", style: .Default) { (action) in
            if let url = NSURL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.sharedApplication().openURL(url)
            }
        }
        alertController.addAction(openAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
